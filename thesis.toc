\select@language {english}
\select@language {greek}
\contentsline {chapter}{\numberline {1}Εισαγωγή}{2}{chapter.1}
\contentsline {section}{\numberline {1.1}Εισαγωγικές έννοιες και βοηθητικοί ορισμοί}{2}{section.1.1}
\contentsline {section}{\numberline {1.2}Εισαγωγή στο δενδροπλάτος}{4}{section.1.2}
\contentsline {chapter}{\numberline {2}Δενδροπλάτος και η σχέση του με άλλες παράμετρους}{7}{chapter.2}
\contentsline {section}{\numberline {2.1}Δενδροπλάτος}{7}{section.2.1}
\contentsline {section}{\numberline {2.2}Partial $k$-trees}{10}{section.2.2}
\contentsline {section}{\numberline {2.3}Chordal graphs}{11}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Intersection γραφήματα και Clique trees}{14}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Σχέσεις μεταξύ των chordal γραφημάτων και του δενδροπλάτους}{15}{subsection.2.3.2}
\contentsline {section}{\numberline {2.4}Elimination Game}{16}{section.2.4}
\contentsline {section}{\numberline {2.5}Διαχωριστής}{19}{section.2.5}
\contentsline {chapter}{\numberline {3}Αλγόριθμοι για το δενδροπλάτος}{24}{chapter.3}
\contentsline {section}{\numberline {3.1}Εισαγωγή στους προσεγγιστικούς αλγορίθμους}{24}{section.3.1}
\contentsline {section}{\numberline {3.2}Πολυωνυμικός προσεγγιστικός αλγόριθμος}{25}{section.3.2}
\contentsline {section}{\numberline {3.3}FPT προσεγγιστικός αλγόριθμος}{27}{section.3.3}
\contentsline {section}{\numberline {3.4}Περισσότεροι αλγόριθμοι για το δενδροπλάτος}{29}{section.3.4}
